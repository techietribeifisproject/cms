package com.Excel.ExcelReadWrite.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Excel.ExcelReadWrite.Repository.RepositoryExcel;
import com.Excel.ExcelReadWrite.model.Test;


@Service
public class ServiceExcel {

	@Autowired
	RepositoryExcel repo;

	public List<Test> save(List<Test> t) {

		t.forEach((a) -> System.out.println(a));
		return repo.saveAll(t);
	}

	public Test saveEntity(Test t) {
		return repo.save(t);
	}
	public List<Test> view() {
		return repo.findAll();
	}
	
	public Optional<Test> viewById(int id){
		return repo.findById(id);
	}
	
	public Test update(Test test) {
		Optional<Test> employee = this.viewById(test.getId());
		Test newEntity = employee.get();
		newEntity.setBatch(test.getBatch());
		newEntity.setDepartment(test.getDepartment());
		newEntity.setId(test.getId());
		newEntity.setMarkSheet1(test.getMarkSheet1());
		newEntity.setMarkSheet2(test.getMarkSheet2());
		newEntity.setMonthAndYear1(test.getMonthAndYear1());
		newEntity.setMonthAndYear2(test.getMonthAndYear2());
		newEntity.setName(test.getName());
		newEntity.setRegNo(test.getRegNo());
		newEntity.setCertificateIssue(test.getCertificateIssue());

		newEntity = repo.save(newEntity);
		
		return newEntity;
	}

	public List<Test> viewByDepartment(String t) {
		return repo.viewByDepartment(t);

	}

	public List<Test> viewByDepartmentCertification(String t) {
		return repo.viewByDepartmentCertification(t);

	}

	public List<Test> viewBySemester(String t) {
		return repo.viewBySemester(t);

	}

	public List<Test> viewBySemester1(String t) {
		return repo.viewBySemester1(t);

	}

}
