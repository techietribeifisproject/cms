package com.Excel.ExcelReadWrite.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Excel.ExcelReadWrite.Repository.RepositoryBatch;
import com.Excel.ExcelReadWrite.Repository.RepositoryExcel;
import com.Excel.ExcelReadWrite.model.BatchAndDepartment;

@Service
public class ServiceBatch {

	@Autowired
	RepositoryBatch repo;
	
	public List<BatchAndDepartment> save(List<BatchAndDepartment> t){
		return repo.saveAll(t);
	}
	
	public List<BatchAndDepartment> view(){
		return repo.findAll();
	}
	
	/*
	 * public List<BatchAndDepartment> viewByDepartment(String t){ return
	 * repo.viewByDepartment(t);
	 * 
	 * }
	 */
	
}
