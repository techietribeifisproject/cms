package com.Excel.ExcelReadWrite.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Excel.ExcelReadWrite.Repository.RepositoryIssue;
import com.Excel.ExcelReadWrite.model.Issue;

@Service
public class ServiceIssue {

	@Autowired
	RepositoryIssue repo;

	public void save(Issue i) {
		repo.save(i);
		
	}

}
