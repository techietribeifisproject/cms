package com.Excel.ExcelReadWrite.Controller;

import java.io.IOException;
import java.lang.ProcessBuilder.Redirect;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.Excel.ExcelReadWrite.Service.ServiceBatch;
import com.Excel.ExcelReadWrite.Service.ServiceExcel;
import com.Excel.ExcelReadWrite.Service.ServiceIssue;
import com.Excel.ExcelReadWrite.model.BatchAndDepartment;
import com.Excel.ExcelReadWrite.model.Issue;
import com.Excel.ExcelReadWrite.model.Test;



@Controller
public class ControllerExcel {

	@Autowired
	ServiceExcel serviceExcel;

	@Autowired
	ServiceBatch serviceBatch;
	
	@Autowired
	ServiceIssue serviceIssue;
	
	@RequestMapping(value = { "/", "home" })
	public String index() {
		System.out.println("Index Page");
		return "index";
	}

	@RequestMapping("/uploaddata")
	public String UploadData() {
		System.out.println("Upload Data Page");
		return "uploaddata";
	}

	@RequestMapping("/viewByDepartment")
	public ModelAndView ViewByDepartments(@ModelAttribute("l") Test t) {
		System.out.println("View Data by Departments Page");
		List<Test> l = serviceExcel.viewByDepartment(t.getName());
		l.forEach((a) -> System.out.println(a));
		return new ModelAndView("viewData", "list", l);
	}

	@RequestMapping("/departments")
	public String departments() {
		System.out.println("Departments Page");
		return "departmentsPage";
	}

	@RequestMapping("/import")
	public String mapReapExcelDatatoDB(@RequestParam("file") MultipartFile reapExcelDataFile) throws IOException {
		System.out.println("Importing file");
		List<Test> tempStudentList = new ArrayList<Test>();
		XSSFWorkbook workbook = new XSSFWorkbook(reapExcelDataFile.getInputStream());
		XSSFSheet worksheet = workbook.getSheetAt(0);

		// --------------------For BatchAndDepartment-------------------------//
		List<BatchAndDepartment> lstbatch = new ArrayList<BatchAndDepartment>();
		BatchAndDepartment batch = new BatchAndDepartment();

		XSSFRow row2 = worksheet.getRow(1);
		XSSFRow row3 = worksheet.getRow(2);
		System.out.println("Department vaues ++++++++++++++++++++++"+row2.getCell(2).getStringCellValue());
		
		System.out.println("Batch vaues ++++++++++++++++++++++"+row3.getCell(2).getStringCellValue());

		batch.setDepartment(row2.getCell(2).getStringCellValue());
		batch.setBatch(row3.getCell(2).getStringCellValue());

		lstbatch.add(batch);

		serviceBatch.save(lstbatch);
		for (int i = 14; i < worksheet.getPhysicalNumberOfRows(); i++) {
			Test tempStudent = new Test();

			XSSFRow row = worksheet.getRow(i);

			/*
			 * tempStudent.setDepartment(row.getCell(1).getStringCellValue());
			 * tempStudent.setBatch(row.getCell(1).getStringCellValue());
			 */

			tempStudent.setRegNo(row.getCell(1).getStringCellValue());
			tempStudent.setName(row.getCell(2).getStringCellValue());
			tempStudent.setMonthAndYear1(row.getCell(3).getStringCellValue());
			tempStudent.setMarkSheet1(row.getCell(4).getStringCellValue());
			tempStudent.setMonthAndYear2(row.getCell(5).getStringCellValue());
			tempStudent.setMarkSheet2(row.getCell(6).getStringCellValue());
			tempStudent.setCertificateIssue((row.getCell(14).getStringCellValue()));
			tempStudent.setBatch(batch.getBatch());
			tempStudent.setDepartment(batch.getDepartment());
			
			tempStudentList.add(tempStudent);
			
		}
		for (Test s : tempStudentList) {
			System.out.println(s.toString());
		}

		for (BatchAndDepartment s : lstbatch) {
			System.out.println(s.toString());
		}
		

//		serviceBatch.save(tempStudentList1);
		serviceExcel.save(tempStudentList);
		return "home";

	}

	@RequestMapping("/view")
	public String view(Model model) {
		List<Test> l = serviceExcel.view();
		model.addAttribute("list", l);
		System.out.println("View Page");
		return "viewData";
	}

	
	
	@RequestMapping("/certificatestatus")
	public String CertificateStatus() {
		System.out.println("Certificate Status Page");
		return "CertificateStatus";
	}
	
	
	@RequestMapping("/semestermarklist")
	public ModelAndView SemesterMarkList() {
		System.out.println("Semester MarkList Page");
		List<Test> li=serviceExcel.view();
		return new ModelAndView("semesterWiseMarkList","list",li);
	}
	
	@RequestMapping("/feedback")
	public String help() {
		System.out.println("FeedBack Form Page");
		return "feedback";
	}
	
	@RequestMapping("/contactus")
	public String contact() {
		System.out.println("Contact Us");
		return "contactUs";
	}

	@RequestMapping(path = {"/edit", "/edit/{id}"})
	public String editById(Model model, @PathVariable("id") Optional<Integer> id) 
							
	{
		if (id.isPresent()) {
			Optional<Test> entity = serviceExcel.viewById(id.get());
			model.addAttribute("employee", entity);
		} else {
			model.addAttribute("employee", new Test());
		}
		System.out.println(id);
		return "editCertificate";
	}
	@RequestMapping(path = "/createExcel", method = RequestMethod.POST)
	public String createOrUpdateExcel(Test employee) 
	{
		serviceExcel.update(employee);
		return "redirect:/certificatestatus";
	}
	
	
	
	
	@RequestMapping("/viewByDepartmentCertification")
	public ModelAndView viewByDepartmentCertification(@ModelAttribute("l") Test t) {
		System.out.println("View Data by Department Certification Page");
		List<Test> l = serviceExcel.viewByDepartmentCertification(t.getName());
		l.forEach((a) -> System.out.println(a));
		return new ModelAndView("certificateIssue", "list", l);
	}
	
	
	
	@RequestMapping("/viewBySemester")
	public ModelAndView viewBySemester(@ModelAttribute("l") Test t) {
		System.out.println("View Data by Department Semester Page");
		List<Test> l = serviceExcel.viewBySemester(t.getMonthAndYear1());
		l.forEach((a) -> System.out.println(a));
		return new ModelAndView("semWiseMark", "list", l);
	}
	
	

	@RequestMapping("/viewBySemester1")
	public ModelAndView viewBySemester1(@ModelAttribute("l") Test t) {
		System.out.println("View Data by Department Semester1 Page");
		List<Test> l = serviceExcel.viewBySemester1(t.getMonthAndYear2());
		l.forEach((a) -> System.out.println(a));
		return new ModelAndView("sem1WiseMark", "list", l);
	}
	
	@RequestMapping("/Certificate")
	public String save(Issue i)
	{
		serviceIssue.save(i);
		return ("redirect:/certificateIssue");
		
	}	
}
