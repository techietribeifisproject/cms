package com.Excel.ExcelReadWrite.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Test {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String regNo;
	private String name;
	private String monthAndYear1;
	private String markSheet1;
	private String monthAndYear2;
	private String markSheet2;
	private String batch;
	private String department;
	private String certificateIssue;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRegNo() {
		return regNo;
	}

	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMonthAndYear1() {
		return monthAndYear1;
	}

	public void setMonthAndYear1(String monthAndYear1) {
		this.monthAndYear1 = monthAndYear1;
	}

	public String getMarkSheet1() {
		return markSheet1;
	}

	public void setMarkSheet1(String markSheet1) {
		this.markSheet1 = markSheet1;
	}

	public String getMonthAndYear2() {
		return monthAndYear2;
	}

	public void setMonthAndYear2(String monthAndYear2) {
		this.monthAndYear2 = monthAndYear2;
	}

	public String getMarkSheet2() {
		return markSheet2;
	}

	public void setMarkSheet2(String markSheet2) {
		this.markSheet2 = markSheet2;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getCertificateIssue() {
		return certificateIssue;
	}

	public void setCertificateIssue(String certificateIssue) {
		this.certificateIssue = certificateIssue;
	}

	public Test(int id, String regNo, String name, String monthAndYear1, String markSheet1, String monthAndYear2,
			String markSheet2, String batch, String department, String certificateIssue) {
		super();
		this.id = id;
		this.regNo = regNo;
		this.name = name;
		this.monthAndYear1 = monthAndYear1;
		this.markSheet1 = markSheet1;
		this.monthAndYear2 = monthAndYear2;
		this.markSheet2 = markSheet2;
		this.batch = batch;
		this.department = department;
		this.certificateIssue = certificateIssue;
	}

	public Test() {
		super();
	}

}
