package com.Excel.ExcelReadWrite.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class BatchAndDepartment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer bid;
	private String batch;
	private String department;
	public Integer getBid() {
		return bid;
	}
	public void setBid(Integer bid) {
		this.bid = bid;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public BatchAndDepartment(Integer bid, String batch, String department) {
		super();
		this.bid = bid;
		this.batch = batch;
		this.department = department;
	}
	public BatchAndDepartment() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "BatchAndDepartment [bid=" + bid + ", batch=" + batch + ", department=" + department + "]";
	}
	
}
