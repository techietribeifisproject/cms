package com.Excel.ExcelReadWrite.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.Excel.ExcelReadWrite.model.Test;

@Repository
public interface RepositoryExcel extends JpaRepository<Test, Integer>  {

	
	@Query(value=" from Test p WHERE p.department=?1 ")
	List<Test> viewByDepartment(String t);

	
	@Query(value=" from Test p WHERE p.department=?1 ")
	List<Test> viewByDepartmentCertification(String t);
	
	
	@Query(value=" from Test p WHERE p.monthAndYear1=?1 ")
	List<Test> viewBySemester(String t);
	
	@Query(value=" from Test p WHERE p.monthAndYear2=?1 ")
	List<Test> viewBySemester1(String t);
	
}
