package com.Excel.ExcelReadWrite.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.Excel.ExcelReadWrite.model.BatchAndDepartment;
import com.Excel.ExcelReadWrite.model.Issue;
import com.Excel.ExcelReadWrite.model.Test;

@Repository
public interface RepositoryIssue extends JpaRepository<Issue, Integer>  {

	
	

}
